'use strict';

/**
 * @ngdoc overview
 * @name baristaApp
 * @description
 * # baristaApp
 *
 * Main module of the application.
 */
angular
  .module('baristaApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactAboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
