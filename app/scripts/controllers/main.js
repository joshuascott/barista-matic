'use strict';

/**
 * @ngdoc function
 * @name baristaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the baristaApp
 */
angular.module('baristaApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
