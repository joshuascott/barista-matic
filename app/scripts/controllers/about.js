'use strict';

/**
 * @ngdoc function
 * @name baristaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the baristaApp
 */
angular.module('baristaApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
